package org.rexberry.rexserverapi.core.persistence;

import org.rexberry.rexserverapi.core.domain.entity.catalog.Game;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface GameRepository {
    Page<Game> findAll(Pageable pageable);
}
