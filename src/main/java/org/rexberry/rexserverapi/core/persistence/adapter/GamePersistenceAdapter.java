package org.rexberry.rexserverapi.core.persistence.adapter;

import lombok.RequiredArgsConstructor;
import org.rexberry.rexserverapi.common.PersistenceAdapter;
import org.rexberry.rexserverapi.core.domain.entity.catalog.Game;
import org.rexberry.rexserverapi.core.persistence.GameRepository;
import org.rexberry.rexserverapi.core.persistence.database.model.GameModel;
import org.rexberry.rexserverapi.core.persistence.repository.GameJpaRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


@PersistenceAdapter
@RequiredArgsConstructor
public class GamePersistenceAdapter implements GameRepository {
    private final GameJpaRepository gameJpaRepository;

    @Override
    public Page<Game> findAll(Pageable pageable) {
        Page<GameModel> gamesPage = this.gameJpaRepository.findAll(pageable);

        return null;
    }
}
