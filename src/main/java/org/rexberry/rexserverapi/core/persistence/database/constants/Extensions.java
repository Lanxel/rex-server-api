package org.rexberry.rexserverapi.core.persistence.database.constants;

public class Extensions {

    public static final class Identifier {
        public static final String GENERATOR = "system-uuid";
        public static final String STRATEGY  = "uuid2";
    }
}