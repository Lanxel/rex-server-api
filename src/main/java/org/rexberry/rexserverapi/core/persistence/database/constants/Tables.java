package org.rexberry.rexserverapi.core.persistence.database.constants;

public class Tables {

    // Table Names
    public static final String MT_GAME = "MT_GAME";

    // Column Sizes
    public static final Integer SIZE_STRING_10  = 10;
    public static final Integer SIZE_STRING_50  = 50;
    public static final Integer SIZE_STRING_100 = 100;
    public static final Integer SIZE_STRING_300 = 300;
}