package org.rexberry.rexserverapi.core.persistence.database.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import org.rexberry.rexserverapi.core.domain.entity.catalog.Game;
import org.rexberry.rexserverapi.core.persistence.database.constants.Tables;
import org.rexberry.rexserverapi.core.persistence.database.constants.Extensions;

import javax.persistence.*;
import java.util.UUID;

@Data
@Builder
@Entity
@Table(name= Tables.MT_GAME)
@NoArgsConstructor
@AllArgsConstructor
public class GameModel {

    @Id
    @GeneratedValue(generator= Extensions.Identifier.GENERATOR)
    @GenericGenerator(name=Extensions.Identifier.GENERATOR, strategy=Extensions.Identifier.STRATEGY)
    private UUID id;

    @Column(length=Tables.SIZE_STRING_100)
    private String name;

    @Column(length=Tables.SIZE_STRING_300)
    private String description;

    public Game toEntity() {
        return Game.builder()
                .id(this.id)
                .name(this.name)
                .description(this.description)
                .build();
    }

    public static GameModel fromEntity(Game game) {
        return GameModel.builder()
                .id(game.getId())
                .name(game.getName())
                .description(game.getDescription())
                .build();
    }
}