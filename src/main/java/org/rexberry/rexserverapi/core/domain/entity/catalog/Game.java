package org.rexberry.rexserverapi.core.domain.entity.catalog;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Game {

    private UUID id;
    private String name;
    private String description;

}
